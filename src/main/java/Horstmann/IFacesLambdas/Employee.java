package Horstmann.IFacesLambdas;

/**
 * Created by host on 07.11.16.
 */
public class Employee implements Measurable{

    public String name;
    public Double salary;

    @Override
    public double getMeasure() {
        return salary;
    }

    public double average(Measurable[]	objects){

        double count = 0.0f;
        for (Measurable m : objects)
        {
            count += m.getMeasure();
        }

        count /= objects.length;

        return count;
    }

    public Measurable largest(Measurable[]	objects){

        if (objects.length == 0)
            return null;

        Measurable ll = objects[0];

        for (Measurable m : objects){
            if (m.getMeasure() > ll.getMeasure())
                ll = m;
        }

        return ll;
    }
}
