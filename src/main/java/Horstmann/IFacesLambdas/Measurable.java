package Horstmann.IFacesLambdas;

/**
 * Created by host on 07.11.16.
 */
public interface Measurable {
        double getMeasure();
}
