package StaticKeyword;

/**
 * * This program is used to show that there is no need of an object * for
 * accessing a static method. It can be called with class name. * @author
 * javawithease
 */

class Display {
	// static method public
	static void display() {
		System.out.println("Hello javawithease.com");
	}
}

public class StaticExample4 {
	public static void main(String args[]) {
		// No need for object to call static method.
		Display.display();
	}
}
