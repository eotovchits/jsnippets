package StaticKeyword;

/**
 * * This program is used to show that static data members * use the same memory
 * locations for all objects. * @author javawithease
 */

class Test {
	static int num = 0;

	// constructor
	Test() {
		num = num + 10;
		System.out.println("Number = " + num);
	}
}

public class StaticExample3 {
	public static void main(String args[]) {
		Test obj1 = new Test();
		Test obj2 = new Test();
		Test obj3 = new Test();
		Test obj4 = new Test();
	}
}
