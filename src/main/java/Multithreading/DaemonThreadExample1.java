package Multithreading;

/**
 * Created by SONY on 11/16/2016.
 */

/**
 * This program is used to show daemon thread example. * @author javawithease
 */
class Test extends Thread {
    public void run() {
        System.out.println(Thread.currentThread().getName() + " " + Thread.currentThread().isDaemon());
    }
}

public class DaemonThreadExample1 {
    public static void main(String args[]) { //creating thread.
        Test thrd1 = new Test();
        Test thrd2 = new Test();
        //set names
        thrd1.setName("My Thread1");
        thrd2.setName("My Thread2");
        //set thrd1 as daemon thread.
        thrd1.setDaemon(true);   //start threads
        thrd1.start();
        thrd2.start();
    }
}