package Lambdas;

/**
 * Created by host on 12.10.16.
 */
public interface EventResult {
    public int onEventOccuranceConfirm(int i);
}
