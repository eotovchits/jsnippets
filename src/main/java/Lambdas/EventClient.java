package Lambdas;

/**
 * Created by host on 12.10.16.
 */
public class EventClient {

    public static void main(String[] args) {

        EventOrganizer eventTester = new EventOrganizer();

        // calling no- argument functional interface

        eventTester.testEvent(() -> System.out.println("event occurred : no argument syntax"));



        // calling single- argument method of functional interface

        eventTester.testEventProcess((i) -> System.out.println("event processed : one argument syntax : " + i));



        // calling alternative single- argument method of functional interface. alternative

        // syntax of omitting parenthesies



        eventTester.testEventProcess(i -> System.out.println("event processed : one argument syntax : " + i));



        // calling alternative single- argument method of functional interface. alternative

        // syntax of wrapping expression body with curly braces

        eventTester.testEventProcess((i) -> {System.out.println("event processed : one argument syntax : " + i);});


        // calling single- argument method of functional interface which that returns

        // the result

        eventTester.testEventResult((i) -> {
            i = i + 1;
            return i;
        });

    }

}
