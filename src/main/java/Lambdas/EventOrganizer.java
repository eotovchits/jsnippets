package Lambdas;

/**
 * Created by host on 12.10.16.
 */
public class EventOrganizer {



    public void testEvent(Event event) {

        event.onEventOccurance();

    }



    public void testEventProcess(EventProcess eventProcess) {

        eventProcess.onEventOccuranceProcess(1);

    }



    public void testEventResult(EventResult eventResult) {

        int result = eventResult.onEventOccuranceConfirm(1);

        // printing the return value of

        System.out.println("Print result value : " + result);

    }

}