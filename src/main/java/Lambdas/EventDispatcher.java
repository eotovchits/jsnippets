package Lambdas;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by host on 12.10.16.
 */
public class EventDispatcher {



    private List<Event> eventList = new ArrayList<Event>();



    private List<EventProcess> eventProcessList = new ArrayList<EventProcess>();



    private List<EventResult> eventResultList = new ArrayList<EventResult>();



    public void registerEventHandler(Event event) {

        eventList.add(event);

    }



    public void registerEventProcessHandler(EventProcess eventProcess) {

        eventProcessList.add(eventProcess);

    }



    public void registerEventResultHandler(EventResult eventResult) {

        eventResultList.add(eventResult);

    }

    public void dispatchEvent() {

        for (Event event : eventList) {

            event.onEventOccurance();

        }

    }



    public void processEvents() {

        int i = 1;

        for(EventProcess process : eventProcessList) {

            process.onEventOccuranceProcess(i++);

        }

    }



    public void compute() {

        final int i = 2;

        for(EventResult process : eventResultList) {

            int result = process.onEventOccuranceConfirm(i);

            System.out.println("return result : "+ result);

        }

    }

}