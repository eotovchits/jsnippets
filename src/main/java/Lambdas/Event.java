package Lambdas;

/**
 * Created by host on 12.10.16.
 *
 * https://community.oracle.com/docs/DOC-1003597
 *
 */
public interface Event {
    public void onEventOccurance();
}
