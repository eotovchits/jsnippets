package thiskeyword;

/**
 * * This program is used to show that this keyword * can be passed as an
 * argument in the constructor.
 * 
 * @author javawithease
 */

class Display8 {
	Display8(Show obj) {
		System.out.println("Show obj = " + obj);
	}
}

class Show {
	Show() { // pass show class object as an argument using this.

		Display8 obj = new Display8(this);
	}
}

public class ThisExample8 {
	public static void main(String args[]) {
		// create Show class object

		Show show = new Show();
	}
}
