package thiskeyword;

/**
 * * This program is used to show that this keyword * can be passed as an
 * argument in the method. * @author javawithease
 */

class Display5 {
	public void displayName() {
		System.out.println("javawithease");

		// passing this keyword as an argument.
		displayObject(this);
	}

	public void displayObject(Display5 obj) { // will print string
												// representation
												// of the object.

		System.out.println(obj);
	}
}

public class ThisExample5 {
	public static void main(String args[]) {

		// create Display class object
		Display5 display = new Display5(); // method call
		display.displayName();
	}
}
