package thiskeyword;

/**
 * * This program is used to show that if local variable and * instance
 * variables are same than compiler will not be able * to distinguish them. This
 * problem is resolved using this keyword. * @author javawithease
 */
class Student1 {
	// instance variable.
	int rollNo;
	String name;

	Student1(String name, int rollNo) {
		// local variable.
		this.name = name;
		this.rollNo = rollNo;
	}

	public void displayDetails() {
		System.out.println("RollNo = " + rollNo);
		System.out.println("name = " + name);
	}
}

public class ThisExample2 {
	public static void main(String args[]) { // creating Student class object.
												 Student1 stu1 = new
												 Student1("jai", 6); //method
												// call 
												 stu1.displayDetails(); }
												}
