package thiskeyword;

/**
 * * This program is used to show that if local variable and * instance
 * variables are different than no need for this keyword. * @author javawithease
 */
class Student3 {
	// instance variable.
	int rollNo;
	String name;

	Student3(String n, int r) {
		// local variable.
		name = n;
		rollNo = r;
	}

	public void displayDetails() {
		System.out.println("RollNo = " + rollNo);
		System.out.println("name = " + name);
	}
}

public class ThisExample3 {
	public static void main(String args[]) { // creating Student class object.
		Student3 stu1 = new Student3("jai", 6); // method
		// call
		stu1.displayDetails();
	}

}
