package thiskeyword;

/**
 * * This program is used to show that this keyword * returns the instance of
 * current class. * @author javawithease
 */
class Display6 {
	public Display6 getDisplay() { // this return the current class object.
		return this;
	}

	public void display() {
		System.out.println("Hello javawbhjgithease.com");
	}
}

public class ThisExample6 {
	public static void main(String args[]) { // create Display class object

		Display6 display = new Display6(); // method call, here getDisplay()
											// returns the //object of current
											// Display class.
		display.getDisplay().display();
	}
}
