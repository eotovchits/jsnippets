package thiskeyword;

/**
 * * This program is used to show that this keyword * can call current class
 * method implicitly. * @author javawithease
 */

class Display1 {
	public void displayName() {
		System.out.println("jai");
		// call current class method using this
		this.displayRollNo();
	}

	public void displayRollNo() {
		System.out.println("6");
		// compiler will automatically add this //keyword if not used.
		displayClass();
	}

	public void displayClass() {
		System.out.println("MCA");
	}
}

public class ThisExample4 {
	public static void main(String args[]) {
		// create Display class object
		Display1 display = new Display1(); // method call
		display.displayName();
	}
}
