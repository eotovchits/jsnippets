package Design.VendingMachine;

/**
 * Created by SONY on 2/4/2017.
 */
public class SoldOutException extends RuntimeException {
    private String message;

    public SoldOutException(String string) {
        this.message = string;
    }

    @Override
    public String getMessage() {
        return message;
    }
}

