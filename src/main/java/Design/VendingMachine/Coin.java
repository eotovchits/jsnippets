package Design.VendingMachine;

/**
 * Created by SONY on 2/4/2017.
 */
public enum Coin {
    PENNY(1), NICKLE(5), DIME(10), QUARTER(25);
    private int denomination;

    private Coin(int denomination) {
        this.denomination = denomination;
    }

    public int getDenomination() {
        return denomination;
    }
}

