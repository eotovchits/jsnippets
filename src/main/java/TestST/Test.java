package TestST;

import java.util.Arrays;

/**
 * Created by host on 13.02.17.
 */
public class Test {
    public static void main(String[] args) {
        System.out.println(Math.min(Double.MIN_VALUE, 0.0d));

        double dd = 1.0 / 0.0;
        System.out.println(dd);

        char[] chars = new char[]{'\u0097'};
        String str = new String(chars);
        byte[] bytes = str.getBytes();
        System.out.println(Arrays.toString(bytes));

        method(null);

        long longWithL = 1000*60*60*24*365L;
        long longWithoutL = 1000*60*60*24*365;
        System.out.println(longWithL);
        System.out.println(longWithoutL);
    }

    public static void method(Object o) {
        System.out.println("Object impl");
    }
    public static void method(String s) {
        System.out.println("String impl");
    }

    /*
    public static void method(StringBuffer i){
        System.out.println("StringBuffer impl");
    }
    */
}

