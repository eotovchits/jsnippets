package AnonymousBlock;

/**
 * * This program is used to show that in which order static * AnonumousBlocks,
 * non-static AnonumousBlocks, super and * default constructors are called.
 * * @author javawithease
 * 
 * Static AnonumousBlock called.
Super class constructor.
Non-Static AnonumousBlock called.
default constructor called.


 */

class Show {
	Show() {
		System.out.println("Super class constructor.");
	}
}

class Display4 extends Show {
	// static Anonymous or instance initializer Block
	static {
		System.out.println("Static AnonumousBlock called.");
	}

	// non-static Anonymous or instance initializer Block
	{
		System.out.println("Non-Static AnonumousBlock called.");
	}

	// default constructor

	Display4() {
		super();
		System.out.println("default constructor called.");
	}
}

public class AnonymousBlockExample4 {
	public static void main(String args[]) {
		Display4 obj = new Display4();
	}
}
