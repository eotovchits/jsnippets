package AnonymousBlock;

/**
 * * This program is used to show that if static and non-static *
 * AnonymousBlocks are used then static AnonymousBlocks is * executed only once.
 * * @author javawithease
 */
class Display3 {
	int a, b;

	// static Anonymous or instance initializer Block
	static {
		System.out.println("Static AnonumousBlock called.");
	}

	// non-static Anonymous or instance initializer Block
	{
		System.out.println("Non-Static AnonumousBlock called.");
		a = 20;
	} // default constructor

	Display3() {
		System.out.println("default constructor called.");
	}

	// one argument constructor
	Display3(int num) {
		System.out.println("one parameter constructor called.");
		b = num;
	}

	// method to display values

	public void display() {
		System.out.println("a = " + a);
		System.out.println("b = " + b);
	}
}

public class AnonymousBlockExample3 {
	public static void main(String args[]) {
		Display3 obj1 = new Display3();
		obj1.display();
		Display3 obj2 = new Display3(30);
		obj2.display();
	}
}
