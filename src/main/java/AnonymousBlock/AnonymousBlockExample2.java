package AnonymousBlock;

/**
 * * This program is used to show that if two AnonymousBlocks * are used then
 * they will execute in the same order in 
 * * which they are appear. * @author
 * javawithease
 */

class Display2 {
	int a, b, c;

	// First Anonymous or instance initializer Block
	{
		System.out.println("First AnonumousBlock called.");
		a = 10;
	}

	// Second Anonymous or instance initializer Block
	{
		System.out.println("Second AnonumousBlock called.");
		b = 20;
	}

	// default constructor
	Display2() {
		System.out.println("default constructor called.");
	} // one argument constructor

	Display2(int num) {
		System.out.println("one parameter constructor called.");
		c = num;
	} // method to display
	// values

	public void display() {
		System.out.println("a = " + a);
		System.out.println("b = " + b);
		System.out.println("c = " + c);
	}
}

public class AnonymousBlockExample2 {
	public static void main(String args[]) {
		Display2 obj1 = new Display2();
		obj1.display();
		Display2 obj2 = new Display2(30);
		obj2.display();
	}

}