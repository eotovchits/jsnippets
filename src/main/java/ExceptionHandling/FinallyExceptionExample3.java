package ExceptionHandling;

/**
 * * This program used to show the use of finally block * when exception occur
 * but not handled. * @author javawithease
 */

class ArithmaticTest7 {
	/**
	 * * This method is used to divide two integers. * @param num1 * @param num2
	 * * @author javawithease
	 */
	public void division(int num1, int num2) {
		try { // java.lang.ArithmeticException here.
			System.out.println(num1 / num2);
		} finally {// It will always execute.
			System.out.println("Finally will always execute.");
		}
		System.out.println("Remaining code after exception handling.");
	}
}

public class FinallyExceptionExample3 {
	public static void main(String args[]) {
		// creating ArithmaticTest object
		ArithmaticTest7 obj = new ArithmaticTest7(); // method call
		obj.division(20, 0);
	}
}
