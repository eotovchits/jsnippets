package ExceptionHandling;

/**
 * This program is used to show that If the super class method declares an
 * exception then subclass overridden method can declare with no exception.
 * 
 * @author javawithease
 */

class SuperClass5 {
	public void display() throws Exception {
		System.out.println("Super class.");
	}
}

class SubClass5 extends SuperClass5 {
	// can declare with no exception.
	public void display() {
		System.out.println("Sub class.");
	}
}

public class ExceptionHandlingInOverriding5 {
	public static void main(String args[]) {
		// Creating subclass object.
		SuperClass5 obj = new SubClass5();

		// method call.
		try {
			obj.display();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
