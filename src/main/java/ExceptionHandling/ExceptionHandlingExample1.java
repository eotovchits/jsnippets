package ExceptionHandling;

/**
 * * This is a simple program which result * into java.lang.ArithmeticException.
 * * @author javawithease
 */

class ArithmaticTest {

	/**
	 * * This method is used to divide two integers. * @param num1 * @param num2
	 */

	public void division(int num1, int num2) {

		// java.lang.ArithmeticException here //and remaining code will not
		// execute.
		int result = num1 / num2;

		// this statement will not execute.
		System.out.println("Division = " + result);
	}
}

public class ExceptionHandlingExample1 {
	public static void main(String args[]) { // creating ArithmaticTest object
		ArithmaticTest obj = new ArithmaticTest(); // method call
		obj.division(20, 0);
	}
}
