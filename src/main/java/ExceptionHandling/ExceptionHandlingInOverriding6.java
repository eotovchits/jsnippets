package ExceptionHandling;

/**
 * * This program is used to show that * If the super class method declares * an
 * exception then subclass overridden * method cannot declare parent exception.
 * * @author javawithease
 */

class SuperClass6 {
	public void display() throws ArithmeticException {
		System.out.println("Super class.");
	}
}

class SubClass6 extends SuperClass6 { // Compile time error here.
	/*
	public void display() throws Exception {
		System.out.println("Sub class.");
	}
	*/
}

public class ExceptionHandlingInOverriding6 {
	public static void main(String args[]) {
		// Creating subclass object.
		SuperClass6 obj = new SubClass6(); // method call.

		try {
			obj.display();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
