package ExceptionHandling;

/**
 * * This program is used to show the use * of Commonly used methods of
 * Throwable class. * @author javawithease
 */

class ArithmaticTestC {
	/**
	 * * This method is used to divide two integers. * @param num1 * @param num2
	 * * @author javawithease
	 */

	public void division(int num1, int num2) {
		try { // java.lang.ArithmeticException here.

			System.out.println(num1 / num2); // catch ArithmeticException here.
		} catch (ArithmeticException e) { // print the message string about the
											// exception.

			System.out.println("getMessage(): " + e.getMessage()); // print the
																	// cause of
																	// the
																	// exception.
			System.out.println("getCause(): " + e.getCause()); // print class
																// name + “: “ +
																// message.
			System.out.println("toString(): " + e.toString());

			System.out.println("printStackTrace(): ");

			// prints the short description of the exception //+ a stack trace
			// for this exception.
			e.printStackTrace();
		}
	}
}

public class ExceptionHandlingExampleC {
	public static void main(String args[]) {

		// creating ArithmaticTest object

		ArithmaticTestC obj = new ArithmaticTestC(); // method call
		obj.division(20, 0);

	}
}