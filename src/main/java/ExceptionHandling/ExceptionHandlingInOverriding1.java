package ExceptionHandling;

import java.io.IOException;

/**
 * * This program is used to show that If * the super class method does not
 * declare * an exception then subclass overridden * method cannot declare the
 * checked exception * @author javawithease
 */

class SuperClass {
	public void display() {
		System.out.println("Super class.");
	}
}

class SubClass extends SuperClass { // Compile time error here.
	/*
	public void display() throws IOException {
		System.out.println("Sub class.");
	}
	*/
}

public class ExceptionHandlingInOverriding1 {
	public static void main(String args[]) { // Creating subclass object.
		SuperClass obj = new SubClass(); // method call.
		obj.display();
	}
}