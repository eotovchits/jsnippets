package ExceptionHandling;

/**
 * * This is a simple program of handling * java.lang.ArithmeticException.
 * * @author javawithease
 */
class ArithmaticTest2 {
	/**
	 * * This method is used to divide two integers. * @param num1 * @param num2
	 * * @author javawithease
	 */

	public void division(int num1, int num2) {
		try {
			// java.lang.ArithmeticException here.
			System.out.println(num1 / num2);

			// catch ArithmeticException here.
		} catch (ArithmeticException e) { // print exception.
			System.out.println(e);
		}
		System.out.println("Remaining code after exception handling.");
	}
}

public class ExceptionHandlingExample2 {
	public static void main(String args[]) {
		// creating ArithmaticTest object

		ArithmaticTest2 obj = new ArithmaticTest2(); // method call
		obj.division(20, 0);
	}
}