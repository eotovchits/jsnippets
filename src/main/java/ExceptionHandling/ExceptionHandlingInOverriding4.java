package ExceptionHandling;

/**
 * * This program is used to show that * If the super class method declares * an
 * exception then subclass overridden * method can declare sub class exception.
 * * @author javawithease
 */

class SuperClass4 {
	public void display() throws Exception {
		System.out.println("Super class.");
	}
}

class SubClass4 extends SuperClass4 {
	// can declare same exception.
	public void display() throws ArithmeticException {
		System.out.println("Sub class.");
	}
}

public class ExceptionHandlingInOverriding4 {
	public static void main(String args[]) { //
		// Creating subclass object.

		SuperClass4 obj = new SubClass4(); // method call.
		try {
			obj.display();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
