package ExceptionHandling;

/**
 * * This program is used to show that * If the super class method does not
 * declare * an exception then subclass overridden * method can declare the
 * unchecked exception * @author javawithease
 */



class SuperClass2 {
	public void display() {
		System.out.println("Super class.");
	}
}

class SubClass2 extends SuperClass2 { // No Compile time error here.
	public void display() throws ArithmeticException {
		System.out.println("Sub class.");
	}
}

public class ExceptionHandlingInOverriding2 {
	public static void main(String args[]) { // Creating subclass object.

		SuperClass2 obj = new SubClass2(); // method call.
		obj.display();
	}
}
