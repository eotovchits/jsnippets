package ExceptionHandling;

/**
 * * This program is used to show that * If the super class method declares * an
 * exception then subclass overridden * method can declare same exception.
 * * @author javawithease
 */

class SuperClass3 {
	public void display() throws ArithmeticException {
		System.out.println("Super class.");
	}
}

class SubClass3 extends SuperClass3 { // can declare same exception.

	public void display() throws ArithmeticException {
		System.out.println("Sub class.");
	}
}

public class ExceptionHandlingInOverriding3 {
	public static void main(String args[]) { // Creating subclass object.
		SuperClass3 obj = new SubClass3(); // method call.
		try {
			obj.display();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
