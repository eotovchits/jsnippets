package ExceptionHandling;

class ArithmaticTest9 {
	/**
	 * * This method is used to divide two integers. * @param num1 * @param num2
	 * * @author javawithease
	 */
	public void division(int num1, int num2) {
		try { // java.lang.ArithmeticException here.
			System.out.println(num1 / num2);
			// catch ArithmeticException here.
		} catch (ArithmeticException e) { // throw exception.
			throw e;
		}
		System.out.println("Remaining code after exception handling.");
	}
}

public class ExceptionThrowExample {
	public static void main(String args[]) {
		// creating ArithmaticTest
		ArithmaticTest9 obj = new ArithmaticTest9(); // method call
		obj.division(20, 0);
	}
}