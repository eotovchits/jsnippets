package ExceptionHandling;

class ArithmaticTest11 {
	/**
	 * This method is used to divide two integers.
	 * 
	 * @param num1
	 * @param num2
	 * @author javawithease
	 */
	public void division(int num1, int num2) throws ArithmeticException {
		// java.lang.ArithmeticException here.
		System.out.println(num1 / num2);
	}

	public void method1(int num1, int num2) throws Exception {
		division(num1, num2);
	}

	public void method2(int num1, int num2) {
		try {
			method1(num1, num2);
		} catch (Exception e) {
			System.out.println("Exception Handled");
		}
	}
}

public class ExceptionThrowsExample {
	public static void main(String args[]) {
		// creating ArithmaticTest object
		ArithmaticTest11 obj = new ArithmaticTest11();

		// method call
		obj.method2(20, 0);
	}
}
