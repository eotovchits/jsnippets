package ExceptionHandling;

/**
 * * This program used to show the use of finally block * when no exception
 * occur. * @author javawithease
 */

class ArithmaticTest8 {
	/**
	 * * This method is used to divide two integers. * @param num1 * @param num2
	 * * @author javawithease
	 */
	public void division(int num1, int num2) {
		try { // java.lang.ArithmeticException here.
			System.out.println(num1 / num2); // catch ArithmeticException here.
		} catch (ArithmeticException e) {
			// print exception.
			System.out.println(e);
		} finally {
			// It will always execute.
			System.out.println("Finally will always execute.");
		}
		System.out.println("Remaining code after exception handling.");
	}
}

public class FinallyExceptionExample1 {
	public static void main(String args[]) {
		// creating ArithmaticTest object
		ArithmaticTest8 obj = new ArithmaticTest8(); // method call
		obj.division(20, 10);
	}
}
