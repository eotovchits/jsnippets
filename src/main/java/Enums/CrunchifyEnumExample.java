package Enums;

public class CrunchifyEnumExample {

	public static void main(String[] args) {
		System.out.println("Get enum value for Comapny 'eBay': " + CrunchifyEnumCompany.EBAY.getCompanyLetter());

		CrunchifyEnumCompany[] arr = CrunchifyEnumCompany.values();
		for (CrunchifyEnumCompany val : arr) {
			System.out.println(val);
		}

		CrunchifyEnumCompany item;
		item = CrunchifyEnumCompany.valueOf("YAHOO");
		System.out.println(item.toString());
		
		for(CrunchifyEnumCompany cat : CrunchifyEnumCompany.values()) {
			System.out.println(cat + " Его порядковое значение " + cat.ordinal());
		}
	}

}