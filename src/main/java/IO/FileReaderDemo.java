package IO;

/**
 * Created by SONY on 2/5/2017.
 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * Java program to read File in Java. It demonstrate two ways by simple example, * one uses java.util.Scanner class and other by using java.io.BufferedReader * class. * * @author http://java67.blogspot.com *
 */
public class FileReaderDemo {
    public static void main(String args[]) throws IOException {
        final String FILE_NAME = "D://Ass_3.txt";

        // 1st way to read File in Java - Using Scanner
        Scanner scnr = new Scanner(new FileInputStream(FILE_NAME));
        while (scnr.hasNextLine()) {
            System.out.println(scnr.nextLine());
        }
        scnr.close();

        // 2nd way to read File in Java - Using BufferedReader
        BufferedReader buffReader = new BufferedReader(new InputStreamReader(new FileInputStream(FILE_NAME)));
        String line = buffReader.readLine();
        while (line != null) {
            System.out.println(line);
            line = buffReader.readLine();
        }
    }
}


