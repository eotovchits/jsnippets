package IO;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by SONY on 2/5/2017.
 */
public class BufferedReaderExample {

    public static void main(String args[]) {

        //reading file line by line in Java using BufferedReader
        FileInputStream fis = null;
        BufferedReader reader = null;

        try {
            fis = new FileInputStream("d:/Ass_3.txt");
            reader = new BufferedReader(new InputStreamReader(fis));

            System.out.println("Reading File line by line using BufferedReader");

            String line = reader.readLine();
            while (line != null) {
                System.out.println(line);
                line = reader.readLine();
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(BufferedReaderExample.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BufferedReaderExample.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                reader.close();
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(BufferedReaderExample.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }


}
