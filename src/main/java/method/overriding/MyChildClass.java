package method.overriding;

class MyBaseClass {
	protected void disp() {
		System.out.println("Parent class method");
	}
}

class MyChildClass extends MyBaseClass {
	public void disp() {
		System.out.println("Child class method");
	}

	public static void main(String args[]) {
		MyBaseClass obj = new MyChildClass();
		obj.disp();
	}
}