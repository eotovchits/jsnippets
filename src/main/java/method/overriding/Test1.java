package method.overriding;

class ABC1 {
	public void mymethod() {
		System.out.println("Class ABC: mymethod()");
	}
}

class Test1 extends ABC1 {
	public void mymethod() {
		// This will call the mymethod() of parent class
		super.mymethod();
		System.out.println("Class Test: mymethod()");
	}

	public static void main(String args[]) {
		Test1 obj = new Test1();
		obj.mymethod();
	}
}