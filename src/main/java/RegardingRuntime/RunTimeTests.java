package RegardingRuntime;

/**
 * Created by host on 14.11.16.
 */
public class RunTimeTests {

    static public void main(String[] args)
    {
        String	className	=	"java.util.Scanner";
        try {
            Class<?>	cl	=	Class.forName(className);

            int aa = 234;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        Class<?>	cl	=	java.util.Scanner.class;

        Class<?>	cl2	=	String[].class;	//	Describes	the	array	type	String[]
        Class<?>	cl3	=	Runnable.class;	//	Describes	the	Runnable	interface
        Class<?>	cl4	=	int.class;	//	Describes	the	int	type
        Class<?>	cl5	=	void.class;	//	Describes	the	void	type

        String str = cl2.getCanonicalName();

        int bb = 345;

        return;
    }
}
