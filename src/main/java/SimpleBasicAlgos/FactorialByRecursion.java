package SimpleBasicAlgos;
public class FactorialByRecursion {
	/**
	 * * This method is used to find factorial of given no. by recursion.
	 * * @param num * @return int
	 */
	static int fact(int num) {
		// Factorial of 0 is 1.
		if (num == 0)
			return 1;
		else
			return num * fact(num - 1);
	}

	public static void main(String args[]) {
		int num = 5, factorial;
		if (num > 0) {
			factorial = fact(num);
			System.out.println("Factorial of " + num + " is " + factorial);
		} else {
			System.out.println("Number should be non negative.");
		}
	}
}
