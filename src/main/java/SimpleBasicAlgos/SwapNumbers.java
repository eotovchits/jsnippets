package SimpleBasicAlgos;
public class SwapNumbers {
	/**
	 * * This method is used to swap no.s without using third variable. * @param
	 * num1 * @param num2
	 */
	static void swapNumbers(int num1, int num2) {
		num1 = num1 + num2;
		num2 = num1 - num2;
		num1 = num1 - num2;
		System.out.println("After swapping: " + num1 + " and " + num2);
	}

	public static void main(String args[]) {
		int num1 = 20;
		int num2 = 30;
		System.out.println("Before swapping:" + num1 + " and " + num2);
		swapNumbers(num1, num2);
	}
}