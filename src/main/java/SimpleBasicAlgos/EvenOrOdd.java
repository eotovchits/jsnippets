package SimpleBasicAlgos;
/**
 * * This program is used to check that given number is even or odd. * @author
 * javawithease
 */
public class EvenOrOdd {
	/**
	 * * This method is used to check that given no is even or odd. * @param num
	 */
	static void evenOdd(int num) {
		if (num % 2 == 0) {
			System.out.println("Given number is even.");
		} else {
			System.out.println("Given number is odd.");
		}
	}

	public static void main(String args[]) { // method call, no need of object.
		evenOdd(123);
	}
}