package stringbuffer;

/**
 * * This program is used to show the use of insert() method. * @author
 * javawithease
 */

class TestStringBuffer1 {
	StringBuffer sb = new StringBuffer("www.com");

	/**
	 * * This method is used to show the use of insert() method. * @author
	 * javawithease
	 */

	public void insertTest() {
		// insert specified string at //the offset indicated position.
		System.out.println(sb.insert(3, ".javawithease"));
	}
}

public class StringBufferInsertExample {
	public static void main(String args[]) {
		// creating TestStringBuffer object
		TestStringBuffer1 obj = new TestStringBuffer1(); // method call
		obj.insertTest();
	}
}
