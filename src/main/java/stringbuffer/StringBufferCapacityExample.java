package stringbuffer;

/**
 * * This program is used to show the use of capacity() method. * @author
 * javawithease
 */

class TestStringBuffer7 {
	StringBuffer sb = new StringBuffer();

	/**
	 * * This method is used to show the use of capacity() method. * @author
	 * javawithease
	 */

	public void capacityTest() { // default capacity.
		System.out.println(sb.capacity());
		sb.append("Hello "); // current capacity 16.

		System.out.println(sb.capacity());
		sb.append("www.javawithease.com");

		// current capacity (16*2)+2=34 i.e (oldcapacity*2)+2.

		System.out.println(sb.capacity());
	}
}

public class StringBufferCapacityExample {

	public static void main(String args[]) { // creating TestStringBuffer object
		TestStringBuffer7 obj = new TestStringBuffer7(); // method call

		obj.capacityTest();
	}
}