package stringbuffer;

import java.util.StringTokenizer;

class TestStringTokenizer { // By default whitespace will act as separator.

	StringTokenizer str = new StringTokenizer("Hello i am here");

	/**
	 * * This method is used to print all tokens of a string. * @author
	 * javawithease
	 */

	public void displayTokens() {
		while (str.hasMoreTokens()) {
			System.out.println(str.nextToken());
		}
	}
}

/**
 * * This program is used to print all tokens * of a string on the bases of
 * comma. * @author javawithease
 */
class TestStringTokenizer2 { // Using comma as separator.

	StringTokenizer str = new StringTokenizer("Hello ,i ,am ,here", ",");

	/**
	 * * This method is used to print all tokens * of a string on the bases of
	 * comma. * @author javawithease
	 */
	public void displayTokens() {
		while (str.hasMoreTokens()) {
			System.out.println(str.nextToken());
		}
	}
}

/**
 * * This program is used to print all tokens of * a string on the bases of
 * multiple separators. * @author javawithease
 */
class TestStringTokenizer3 { // Using multiple separators.
	StringTokenizer str = new StringTokenizer("Hello ,i ;am :here", ", ; :");

	/**
	 * * This method is used to print all tokens of * a string on the bases of
	 * multiple separators. * @author javawithease
	 */
	public void displayTokens() {
		while (str.hasMoreTokens()) {
			System.out.println(str.nextToken());
		}
	}
}

public class StringTokenizerExample {
	public static void main(String args[]) {
		// creating TestStringTokenizer object.
		TestStringTokenizer obj = new TestStringTokenizer(); // method call
		obj.displayTokens();

		TestStringTokenizer2 obj2 = new TestStringTokenizer2(); // method call
		obj2.displayTokens();

		TestStringTokenizer3 obj3 = new TestStringTokenizer3(); // method call
		obj3.displayTokens();
	}
}