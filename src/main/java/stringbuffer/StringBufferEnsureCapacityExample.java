package stringbuffer;

/**
 * * This program is used to show the use of ensureCapacity() method. * @author
 * javawithease
 */

class TestStringBuffer8 {
	StringBuffer sb = new StringBuffer();

	/**
	 * * This method is used to show the use of ensureCapacity() method.
	 * * @author javawithease
	 */

	public void ensureCapacityTest() { // default capacity.

		System.out.println(sb.capacity());
		sb.append("Hello "); // current capacity 16.

		System.out.println(sb.capacity());
		sb.append("www.javawithease.com");
		// current capacity (16*2)+2=34 i.e (oldcapacity*2)+2.

		System.out.println(sb.capacity());
		sb.ensureCapacity(10);

		// now no change in capacity because //minimum is already set to 34.

		System.out.println(sb.capacity());
		sb.ensureCapacity(50);

		// now (34*2)+2 = 70 as 50 is greater than 34.
		System.out.println(sb.capacity());
	}
}

public class StringBufferEnsureCapacityExample {
	public static void main(String args[]) {

		// creating TestStringBuffer object
		TestStringBuffer8 obj = new TestStringBuffer8(); // method call
		obj.ensureCapacityTest();
	}
}