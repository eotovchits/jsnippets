package stringbuffer;

/**
 * * This program is used to show the use of delete() method. * @author
 * javawithease
 */

class TestStringBuffer4 {
	StringBuffer sb = new StringBuffer("Hello www.javawithease.com");

	/**
	 * * This method is used to show the use of delete() method. * @author
	 * javawithease
	 */

	public void deleteTest() { // delete the substring of the string
		// buffer from startIndex to endIndex-1.
		System.out.println(sb.delete(0, 6));
	}
}

public class StringBufferDeleteExample {
	public static void main(String args[]) { // creating TestStringBuffer4
												// object
		TestStringBuffer4 obj = new TestStringBuffer4();
		// method call
		obj.deleteTest();
	}
}
