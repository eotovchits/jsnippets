package stringbuffer;

/**
 * * This program is used to show the use of replace() method. * @author
 * javawithease
 */

class TestStringBuffer2 {
	StringBuffer sb = new StringBuffer("www.abc.com");

	/**
	 * * This method is used to show the use of replace() method. * @author
	 * javawithease
	 */

	public void replaceTest() { // replace the substring of the string buffer
								// from
		// startIndex to endIndex-1 with specified string.
		System.out.println(sb.replace(4, 7, "javawithease"));
	}
}

public class StringBufferReplaceExample {
	public static void main(String args[]) {
		// creating TestStringBuffer object
		TestStringBuffer2 obj = new TestStringBuffer2(); // method call
		obj.replaceTest();
	}
}
