package stringbuffer;

/**
 * * This program is used to show the use of reverse() method. * @author
 * javawithease
 */
class TestStringBuffer6 {
	StringBuffer sb = new StringBuffer("www.javawithease.com");

	/**
	 * * This method is used to show the use of reverse() method. * @author
	 * javawithease
	 */

	public void reverseTest() {
		// replace the string buffer’s character //sequence by reverse character
		// sequence.
		System.out.println(sb.reverse());
	}
}

public class StringBufferReverseExample {
	public static void main(String args[]) {
		// creating TestStringBuffer object
		TestStringBuffer6 obj = new TestStringBuffer6(); // method call
		obj.reverseTest();
	}
}