package stringbuffer;

/**
 * * This program is used to show the use of append() method. * @author
 * javawithease
 */

class TestStringBuffer {
	StringBuffer sb = new StringBuffer("Hello ");

	/**
	 * * This method is used to show the use of append() method. * @author
	 * javawithease
	 */

	public void appendTest() {
		// concatenate the argument string
		// at the end of this string.
		System.out.println(sb.append("www.javawithease.com"));
	}
}

public class StringBufferAppendExample {
	public static void main(String args[]) {

		// creating TestStringBuffer object
		TestStringBuffer obj = new TestStringBuffer(); // method call
		obj.appendTest();
	}
}
