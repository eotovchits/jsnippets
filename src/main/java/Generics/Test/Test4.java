package Generics.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by host on 15.12.16.
 *
 * http://www.quizful.net/post/java-generics-tutorial
 */
class Test4 {
    static void printList(List<?> list) {
        for (Object l : list)
            System.out.println("{" + l + "}");
    }

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(10);
        list.add(100);
        printList(list);
        List<String> strList = new ArrayList<>();
        strList.add("10");
        strList.add("100");
        printList(strList);

        List<?> numList = new ArrayList<Integer>();
        numList = new ArrayList<String>();

        List<? extends Number> numList1 = new ArrayList<Integer>();
        //numList1 = new ArrayList<String>();
        numList1 = new ArrayList<Double>();


    }

    public static Double sum(List<? extends Number> numList) {
        Double result = 0.0;
        for (Number num : numList) {
            result += num.doubleValue();
        }

        List<? super Integer> intList = new ArrayList<Integer>();
        System.out.println("The intList is: " + intList);

        return result;
    }
}
