package Generics.Test;

/**
 * Created by host on 15.12.16.
 * |
 *
 *
 * https://docs.oracle.com/javase/tutorial/java/generics/types.html
 */
interface Pair1<K, V> {
    public K getKey();
    public V getValue();
}

public class OrderedPair<K, V> implements Pair1<K, V> {

    private K key;
    private V value;

    public OrderedPair(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K getKey()	{ return key; }
    public V getValue() { return value; }
}

//Pair1<String, Integer> p1 = new OrderedPair<String, Integer>("Even", 8);
//Pair1<String, String>  p2 = new OrderedPair<String, String>("hello", "world");

//OrderedPair<String, Integer> p1 = new OrderedPair<>("Even", 8);
//OrderedPair<String, String>  p2 = new OrderedPair<>("hello", "world");