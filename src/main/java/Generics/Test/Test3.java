package Generics.Test;

/**
 * Created by host on 15.12.16.
 */
import java.util.ArrayList;
import java.util.List;

class Utilities {
    public static <T> void fill(List<T> list, T val) {
        for (int i = 0; i < list.size(); i++)
            list.set(i, val);
    }
}

class Test3 {
    public static void main(String[] args) {
        List<Integer> intList = new ArrayList<Integer>();
        intList.add(1);
        intList.add(2);
        System.out.println("Список до обработки дженерик-методом: " + intList);
        Utilities.fill(intList, 0);
        System.out.println("Список после обработки дженерик-методом: "
                + intList);

        //List<Integer> list1 = new List<Integer>();
        List<Integer> list2 = new ArrayList<Integer>();
        //List<Number> list3 = new ArrayList<Integer>();
        //List<Integer> list4 = new ArrayList<Number>();

        List<?> List5 = new ArrayList<Integer>();
    }
}