package Generics;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * https://www.javacodegeeks.com/2013/07/java-generics-tutorial-example-class-interface-methods-wildcards-and-much-more.html
 * https://www.javacodegeeks.com/2013/07/java-generics-tutorial-example-class-interface-methods-wildcards-and-much-more.html
 * https://www.javacodegeeks.com/2013/07/java-generics-tutorial-example-class-interface-methods-wildcards-and-much-more.html
 *
 * Created by SONY on 11/28/2016.

 https://www.javacodegeeks.com/2013/12/subtyping-in-java-generics.html
 https://www.javacodegeeks.com/2012/08/dao-layer-generics-to-rescue.html
 https://www.javacodegeeks.com/2012/07/java-generics-interview-questions.html
 https://www.javacodegeeks.com/2014/03/synthetic-and-bridge-methods.html
 https://www.javacodegeeks.com/2014/02/coping-with-methods-with-many-parameters.html
 https://www.javacodegeeks.com/2013/12/advanced-java-generics-retreiving-generic-type-arguments.html
 https://www.javacodegeeks.com/2015/03/polymorphism-in-java-generics.html

 https://www.javacodegeeks.com/2014/07/an-introduction-to-generics-in-java-part-6.html

 https://www.javacodegeeks.com/2014/07/template-method-pattern-example-using-java-generics.html
 https://examples.javacodegeeks.com/java-basics/generics/generic-method-example-in-java/

 http://www.java67.com/2016/01/how-to-implement-singly-linked-list-in-java-using-generics-example.html



 */
public class Sample {

    public static void main(String[] argv) {
        List list = new ArrayList();
        list.add("abc");
        list.add(new Integer(5)); //OK

        for (Object obj : list) {
            String str = (String) obj; //type casting leading to ClassCastException at runtime
        }
    }
}
