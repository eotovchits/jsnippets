package Generics;

/**
 * Created by SONY on 11/28/2016.
 */
public class TestA<T extends Comparable<T>> {

    private T data;
    private TestA<T> next;

    public TestA(T d, TestA<T> n) {
        this.data = d;
        this.next = n;
    }

    public T getData() {
        return this.data;
    }
}