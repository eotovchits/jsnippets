package Generics;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SONY on 11/28/2016.
 */
public class GenericsWildcards {

    public static void main(String[] args) {
        List<Integer> ints = new ArrayList<>();
        ints.add(3);
        ints.add(5);
        ints.add(10);
        double sum = sum(ints);
        System.out.println("Sum of ints=" + sum);
    }

    public static void addIntegers(List<? super Integer> list){
        list.add(new Integer(50));
    }

    public static double sum(List<? extends Number> list) {
        double sum = 0;
        for (Number n : list) {
            sum += n.doubleValue();
        }

        List<? extends Integer> intList = new ArrayList<>();
        List<? extends Number>  numList = intList;  // OK. List<? extends Integer> is a subtype of List<? extends Number>

        return sum;
    }

    public static void printData(List<?> list){
        for(Object obj : list){
            System.out.print(obj + "::");
        }
    }
}