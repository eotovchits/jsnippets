package Generics;

/**
 * Created by host on 21.11.16.
 */
public class T1 {

    public static void main (String args[]){

        Entry<String,	Integer>	entry	=	new	Entry<>("Fred",	42);

        String[]	friends	=	{"aaa1", "aaa2", "aaa3"};

        Arrays.swap(friends,	0,	1);

        System.out.println(friends[0]);
        System.out.println(friends[1]);
        System.out.println(friends[2]);

        Arrays.<String>swap(friends,	1,	2);

        System.out.println(friends[0]);
        System.out.println(friends[1]);
        System.out.println(friends[2]);
    }
}
