package HashCode;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by SONY on 2/5/2017.
 * <p>
 * http://www.java67.com/2013/04/example-of-overriding-equals-hashcode-compareTo-java-method.html
 */
public class EqualsAndHashCodeTest {
    @Test
    public void testEquals() {
        Person james = new Person("James", 21, new Date(1980, 12, 1));
        Person same = new Person("James", 21, new Date(1980, 12, 1));
        Person similar = new Person("Harry", 21, new Date(1981, 12, 1));
        assertTrue(james.equals(same));
        assertTrue(james.hashCode() == same.hashCode());
        assertFalse(james.equals(null));
        assertFalse(james.equals(similar));
        assertTrue(james.hashCode() != similar.hashCode());
    }
}

