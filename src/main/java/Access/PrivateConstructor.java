package Access;

/**
 * This program is used to show that we cannot create an instance of that class
 * from outside the class if constructor is private.
 * 
 * @author javawithease
 */
class Student9 {
	// private constructor of the class
	private Student9() {

	}

	public void show() {
		System.out.println("Hello javawithease.com");
	}
}

public class PrivateConstructor {
	public static void main(String args[]) {
		// compile time error in creating Student class object
		// because of private constructor.
		/// Student obj = new Student();
	}
}
