package Access;

/**
 * * This program is used to show that members of the class * of a class can be
 * accessed in the package level only. * @author javawithease
 */
class Student1 { // private members of the class
	int rollNo = 5;

	void showRollNo() {
		System.out.println("RollNo = " + rollNo);
	}
}

public class DefaultAccessModifier1 {
	public static void main(String args[]) { // creating Student class object
		Student1 obj = new Student1();
		// //No compile time error
		// because members of the class
		// //of a class can be accessed
		// in that package but can't be
		// //accessed outside the
		// package.
		System.out.println(obj.rollNo);
		obj.showRollNo();
	}
}