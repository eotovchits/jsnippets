package Reflection;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;

/**
 * Created by host on 15.11.16.
 */
public class Test1 {

    public static void main(String args[]){

        String	className	=	"java.util.Scanner";

        Class<?>	cl	= null;
        try {
            cl = Class.forName(className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        while	(cl	!=	null)	{
            for	(Method m	:	cl.getDeclaredMethods())	{
                System.out.println(
                        Modifier.toString(m.getModifiers())	+	" " +
                        m.getReturnType().getCanonicalName()	+	" " +
                        m.getName()	+
                        Arrays.toString(m.getParameters()));
            }
            cl	=	cl.getSuperclass();
        }

    }
}
