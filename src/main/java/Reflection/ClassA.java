package Reflection;

/**
 * ClassA
 * @author Pierre-Hugues Charbonneau
 *
 */
public class ClassA {

    private final static Class<ClassA> CLAZZ = ClassA.class;

    static {
        System.out.println("Class loading of "+CLAZZ+" from ClassLoader '"+CLAZZ.getClassLoader()+"' in progress...");
    }

    public ClassA() {
        System.out.println("Creating a new instance of "+ClassA.class.getName()+"...");

        doSomething();
    }

    private void doSomething() {
        // Nothing to do...
    }
}