package Reflection;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;

/**
 * Created by host on 15.11.16.
 */
public class ProxyTest {

    public static void main(String args[]){

        Object[]	values	=	new	Object[1000];
        for	(int	i	=	0;	i	<	values.length;	i++)	{
            Object	value	=	new	Integer(i);
            values[i]	=	Proxy.newProxyInstance(
                    null,
                    value.getClass().getInterfaces(),
                    //	Lambda	expression	for	invocation	handler
                    (Object proxy, Method m, Object[] margs) -> {
                        System.out.println(value +    "."+m.getName() +
                                Arrays.toString(margs));
                        return m.invoke(value, margs);
                    });
        }

        Arrays.binarySearch(values,	new	Integer(500));

    }
}
