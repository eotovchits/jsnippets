package Reflection;

/* http://tutorials.jenkov.com/java-reflection/classes.html */

import java.lang.reflect.Modifier;

/**
 * Created by SONY on 2/28/2016.
 */
public class Classes {

    public static void main(String argv[])
    {
        String className = "Reflection.MyObject";

        Class aClass = null;
        try {
             aClass = Class.forName(className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        String className1 = aClass.getName();
        String simpleClassName = aClass.getSimpleName();

        System.out.println(className);
        System.out.println(className1);

        System.out.println(simpleClassName);

        int modifiers = aClass.getModifiers();

        System.out.println(Modifier.isAbstract(modifiers));
        System.out.println(Modifier.isFinal(modifiers));
        System.out.println(Modifier.isInterface(modifiers));
        System.out.println(Modifier.isNative( modifiers));
        System.out.println(Modifier.isPrivate( modifiers));
        System.out.println(Modifier.isProtected( modifiers));
        System.out.println(Modifier.isPublic( modifiers));
        System.out.println(Modifier.isStatic( modifiers));
        System.out.println(Modifier.isStrict( modifiers));
        System.out.println(Modifier.isSynchronized( modifiers));
        System.out.println(Modifier.isTransient( modifiers));
        System.out.println(Modifier.isVolatile( modifiers));

        Package pack = aClass.getPackage();

    }
}
