package Super;

/**
 * * This program is used to show the use super keyword * to invoke the super
 * class method from subclass method. * @author javawithease
 */

class Display3 {
	public void display() {
		System.out.println("display method of super class.");
	}
}

class Show3 extends Display3 {
	public void display() {
		System.out.println("display method of sub class.");
	}

	public void show() {
		System.out.println("show method of sub class.");

		// super class display method is called.
		super.display();
	}
}

public class SuperExample3 {
	public static void main(String args[]) {
		// create Show class object.
		Show3 obj = new Show3(); // method call
		obj.show();
	}
}
