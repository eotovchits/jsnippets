package OOP;

/**
 * This program is used for simple method overriding example. * with dynamic method dispatch. * @author javawithease
 */
class Student1 {
    int maxRollNo = 200;
}

class SchoolStudent extends Student1 {
    int maxRollNo = 120;
}

class CollegeStudent1 extends SchoolStudent {
    int maxRollNo = 100;
}

public class StudentTest {
    public static void main(String args[]) {
        //Super class can contain subclass object.
        Student1 obj1 = new CollegeStudent1();
        Student1 obj2 = new SchoolStudent();
        //In both calls maxRollNo of super class will be printed.
        System.out.println(obj1.maxRollNo);
        System.out.println(obj2.maxRollNo);
    }
}