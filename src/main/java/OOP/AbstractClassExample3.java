package OOP;

/**
 * This program is used to show that abstract class can have both static * and non-static data members and methods like any other java class. * @author javawithease
 */
abstract class GraphicObjects2 {
    //non static data member
    int var1 = 50;
    //static data member
    static String str1 = "www.javawithease.com";

    //abstract method declaration
    abstract void showShape();

    //non abstract, non static method
    void area(int area) {
        System.out.println("Area = " + area);
    }

    //non abstract, static method
    static void displayGraphicObjects() {
        System.out.println("Graphic objects.");
    }
}

class Circle2 extends GraphicObjects2 {
    /**
     * This is the overridden method, provide implementation * of abstract method according to your need. * @author javawithease
     */
    void showShape() {
        System.out.println("Object type is Circle.");
        System.out.println("Non static variable = " + var1);
    }
}

public class AbstractClassExample3 {
    public static void main(String args[]) {
        //GraphicObjects is the super class
        // hence it's reference can contain subclass object.
        GraphicObjects2 obj = new Circle2();
        obj.showShape();
        obj.area(250);
        //call static method and variable with class name.
        GraphicObjects2.displayGraphicObjects();
        System.out.println("static variable = " + GraphicObjects2.str1);
    }
}