package OOP;

/** * This class is used add and show car details. * @author javawithease */
public class Car {
	String carColor;
	int carSpeed;

	/**
	 * * This method is used to add car details. * @param color * @param speed
	 * * @author javawithease
	 */
	void addCarDetails(String color, int speed) {
		carColor = color;
		carSpeed = speed;
	}

	/** * This method is used to show details. * @author javawithease */
	void showCarDetails() {
		System.out.println("Color: " + carColor);
		System.out.println("Speed: " + carSpeed);
	}

	public static void main(String args[]) {
		// creating objects
		Car car1 = new Car();
		Car car2 = new Car();
		// //method call, need object here because method is non-static.
		car1.addCarDetails("white", 120);
		car2.addCarDetails("Red", 150);
		car1.showCarDetails();
		car2.showCarDetails();
	}
}
