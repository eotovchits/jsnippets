package OOP;

/**
 * This program is used to show simple use of abstract class. * @author javawithease
 */
abstract class GraphicObjects {
    //abstract method declaration
    abstract void showShape();
}

class Circle extends GraphicObjects {
    /**
     * This is the overridden method, provide implementation * of abstract method according to your need. * @author javawithease
     */
    void showShape() {
        System.out.println("Object type is Circle.");
    }
}

class Rectangle extends GraphicObjects {
    /**
     * This is the overridden method, provide implementation * of abstract method according to your need. * @author javawithease
     */
    void showShape() {
        System.out.println("Object type is Rectangle.");
    }
}

class Triangle extends GraphicObjects {
    /**
     * This is the overridden method, provide implementation * of abstract method according to your need. * @author javawithease
     */
    void showShape() {
        System.out.println("Object type is Triangle.");
    }
}

public class AbstractClassExample1 {
    public static void main(String args[]) {
        //GraphicObjects is the super class
        // hence it's reference can contain subclass object.
        GraphicObjects obj = new Circle();
        obj.showShape();
        obj = new Rectangle();
        obj.showShape();
        obj = new Triangle();
        obj.showShape();
    }
}
