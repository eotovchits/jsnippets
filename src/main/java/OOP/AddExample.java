package OOP;

/**
 * * This class is used for method overloading * by parameters change in their
 * types. * @author javawithease
 */
public class AddExample {
	/**
	 * * This method is used to add two integer values. * @param var1 * @param
	 * var2 * @author javawithease
	 */
	void add(int var1, int var2) {
		System.out.println(var1 + var2);
	}

	/**
	 * * This method is used to add one double and one integer. * @param var1
	 * * @param var2 * @author javawithease
	 */
	void add(double var1, int var2) {
		System.out.println(var1 + var2);
	}

	/**
	 * * This method is used to concatenate two string values. * @param var1
	 * * @param var2 * @author javawithease
	 */
	void add(String var1, String var2) {
		System.out.println(var1 + var2);
	}

	public static void main(String args[]) { // creating object here
		AddExample addExample = new AddExample(); // method call
		addExample.add(10, 20);
		addExample.add(12.50, 30);
		addExample.add("hello ", "java.");
	}
}
