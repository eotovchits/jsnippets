package OOP;

/**
 * This program is used to show simple interface example. * @author javawithease
 */
interface ShowDetails {
    //This method is used to print name and age.
    void showDetail(String name, int age);
}

public class InterfaceExample1 implements ShowDetails {
    /**
     * This method is used to print name and age. * @author javawithease
     */
    @Override
    public void showDetail(String name, int age) {
        System.out.println("Name = " + name);
        System.out.println("Age = " + age);
    }

    public static void main(String args[]) {
        //object creation
        InterfaceExample1 obj = new InterfaceExample1();
        //method call
        obj.showDetail("jai", 26);
    }
}
