package OOP;

import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * This program is used to read input using Scanner Class. * @author javawithease
 */
public class ReadInputUsingScanner {
    public static void main(String args[]) {
        //create a scanner class object.
        Scanner scanner = new Scanner(new InputStreamReader(System.in));
        System.out.println("Enter your full name: ");
        //read a line using scanner object.
        String userName = scanner.nextLine();
        System.out.println("Enter your full age: ");
        //read an integer using scanner object.
        int age = scanner.nextInt();
        //print input values
        System.out.println("User name : " + userName);
        System.out.println("User age : " + age);
    }
}