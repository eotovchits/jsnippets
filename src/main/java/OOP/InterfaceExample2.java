package OOP;

/**
 * This program is used to show multiple inheritance example. * @author javawithease
 */
interface ShowAge {
    //This method is used to print age.
    void age(int age);
}

interface ShowName {
    //This method is used to print name.
    void name(String name);
}

public class InterfaceExample2 implements ShowAge, ShowName {
    /**
     * This method is used to print age. * @author javawithease
     */
    @Override
    public void age(int age) {
        System.out.println("Age = " + age);
    }

    /**
     * This method is used to print name. * @author javawithease
     */
    @Override
    public void name(String name) {
        System.out.println("Name = " + name);
    }

    public static void main(String args[]) {
        //object creation
        InterfaceExample2 obj = new InterfaceExample2();
        //method call
        obj.name("jai");
        obj.age(26);
    }
}
