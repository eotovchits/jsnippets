package OOP;

/**
 * This program is used for Hierarchical inheritance example.
 *
 * @author javawithease
 */
class Numbers {
    int a = 10;
    int b = 20;
}

class AddNumbers extends Numbers {
    /**
     * This method is used to add.
     *
     * @author javawithease
     */
    public void showAdd() {
        System.out.println(a + b);
    }
}

class MultiplyNumbers extends Numbers {
    /**
     * This method is used to multiply.
     *
     * @author javawithease
     */
    public void showMultiplication() {
        System.out.println(a * b);
    }
}

public class Test {
    public static void main(String args[]) {
        //creating base classes objects
        AddNumbers obj1 = new AddNumbers();
        MultiplyNumbers obj2 = new MultiplyNumbers();

        //method calls
        obj1.showAdd();
        obj2.showMultiplication();
    }
}
