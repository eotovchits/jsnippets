package OOP;

/**
 * * This class is used to show ambiguity problem * in case of change in return
 * type of the method. * @author javawithease
 */
public class AmbiguityInOverloading {
	/**
	 * * This method is used to add two numerics. * @param num1 * @param num2
	 * * @return int * @author javawithease
	 */
	int add(int num1, int num2) {
		return num1 + num2;
	}

	/**
	 * * This method is used to add two numerics. * @param num1 * @param num2
	 * * @return float * @author javawithease
	 */
	/*
	float add(int num1, int num2) {
		return num1 + num2;
	}
	*/
	public static void main(String args[]) { // creating object here
		AmbiguityInOverloading obj = new AmbiguityInOverloading();
		// compiler can't
		// differentiate method call
		System.out.println(obj.add(10, 20));
	}
}
