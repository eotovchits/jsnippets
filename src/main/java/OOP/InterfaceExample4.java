package OOP;

/**
 * This program is used to show that an interface * can extends multiple interfaces. * @author javawithease
 */
interface ShowAge1 {
    //This method is used to print age.
    void age(int age);
}

interface ShowName1 {
    //This method is used to print name.
    void name(String name);
}

interface showDetails extends ShowAge1, ShowName1 {
    //This method is used to print roll no.
    void rollNo(int rollNo);
}

public class InterfaceExample4 implements showDetails {
    /**
     * This method is used to print age. * @author javawithease
     */
    @Override
    public void age(int age) {
        System.out.println("Age = " + age);
    }

    /**
     * This method is used to print name. * @author javawithease
     */
    @Override
    public void name(String name) {
        System.out.println("Name = " + name);
    }

    /**
     * This method is used to print rollNo. * @author javawithease
     */
    @Override
    public void rollNo(int rollNo) {
        System.out.println("RollNo = " + rollNo);
    }

    public static void main(String args[]) {
        //object creation
        InterfaceExample4 obj = new InterfaceExample4();
        //method call
        obj.name("jai");
        obj.age(26);
        obj.rollNo(4);
    }
}