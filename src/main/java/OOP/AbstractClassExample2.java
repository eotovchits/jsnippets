package OOP;

/**
 * This program is used to show that a class either * have to provide implementation * of all abstract methods of extended abstract * class or declare abstract itself. * @author javawithease
 */
abstract class GraphicObjects1 {
    //abstract method declaration
    abstract void showShape();
}

class Circle1 extends GraphicObjects1 {
    /**
     * This is the overridden method, provide implementation * of abstract method according to your need. * @author javawithease
     */
    void showShape() {
        System.out.println("Object type is Circle.");
    }
}
/*
class Rectangle1 extends GraphicObjects1 {
    //error here, Rectangle class have to provide implementation
    // of all abstract methods of extended abstract class.
}
*/
abstract class Triangle1 extends GraphicObjects1 {
    //no error here, because Triangle class is declared
    // as an abstract class
}

public class AbstractClassExample2 {
    public static void main(String args[]) {
        //GraphicObjects is the super class
        // hence it's reference can contain subclass object.
        GraphicObjects1 obj = new Circle1();
        obj.showShape();
    }
}