package OOP;

/**
 * This program is used to show multiple inheritance is * possible in case of interfaces because there * is no problem of ambiguity. * @author javawithease
 */
interface Show {
    //This method is used to print name and age.
    void show(String name, int age);
}

interface Detail {
    //This method is used to print name and age.
    void show(String name, int age);
}

public class InterfaceExample3 implements Show, Detail {
    @Override
    public void show(String name, int age) {
        System.out.println("Name = " + name);
        System.out.println("Age = " + age);
    }

    public static void main(String args[]) {
        //object creation
        InterfaceExample3 obj = new InterfaceExample3();
        //method call
        obj.show("jai", 26);
    }
}