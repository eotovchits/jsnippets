package OOP;

/**
 * This program is used for simple method overriding example. * @author javawithease
 */
class Student {
    /**
     * This method is used to show details of a student. * @author javawithease
     */
    public void show() {
        System.out.println("Student details.");
    }
}

public class CollegeStudent extends Student {
    /**
     * This method is used to show details of a college student. * @author javawithease
     */
    public void show() {
        System.out.println("College Student details.");
    }

    //main method
    public static void main(String args[]){
        Student obj = new CollegeStudent();
        //subclass overrides super class method
        // hence method of CollegeStudent class will be called.
        obj.show();
    }
}