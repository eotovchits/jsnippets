package display;

/** * This class is used to display entered text. * @author javawithease */
public class Display {
	public static void displayText(String text) {
		System.out.println(text);
	}
}