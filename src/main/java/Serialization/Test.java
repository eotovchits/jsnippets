package Serialization;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Created by SONY on 1/21/2017.
 */
public class Test implements Serializable {

    public static final long serialVersionUID = 1234L;

    public static void main(String arg[]){
        String filePath = "data.ser";
        String message = "Java Serialization is Cool";
        Test1 ttt = new Test1();

        try (
                FileOutputStream fos = new FileOutputStream(filePath);
                ObjectOutputStream outputStream = new ObjectOutputStream(fos);
        ) {

            outputStream.writeObject(message);
            outputStream.writeObject(ttt);

        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

}
