package Strings;

/**
 * * This program is used to create a immutable class. * @author javawithease
 */

final class Student {
	// declare all properties final.
	final String rollNo;

	public Student(String rollNo) {
		this.rollNo = rollNo;
	}

	// only create getter method.

	public String getRollNo() {
		return rollNo;
	}
}

public class ImmutableClassExample {
	public static void main(String args[]) {

		// creating Student object.
		Student obj = new Student("MCA/07/06");
		System.out.println(obj.getRollNo());
	}
}