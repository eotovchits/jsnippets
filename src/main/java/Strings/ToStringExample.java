package Strings;

/**
 * * This program is used to show the use of toString method. * @author
 * javawithease
 */

class Student2 {
	String name;
	String rollNo;

	// constructor
	Student2(String name, String rollNo) {
		this.name = name;
		this.rollNo = rollNo;
	}
}

class Student3 {
	String name;
	String rollNo;

	// constructor
	Student3(String name, String rollNo) {
		this.name = name;
		this.rollNo = rollNo;
	}

	// Override toString method to get customize results.

	public String toString() {
		return "Name:" + name + ", RollNo: " + rollNo;
	}
}

public class ToStringExample {
	public static void main(String args[]) {
		// creating Student class object

		Student2 stu1 = new Student2("Sunil", "MCA/07/15");
		Student2 stu2 = new Student2("Sandy", "MCA/07/19");
		Student2 stu3 = new Student2("Roxy", "MCA/07/32");

		// println internally call toString method
		System.out.println(stu1);
		System.out.println(stu2);
		System.out.println(stu3);

		Student3 stu11 = new Student3("Sunil", "MCA/07/15");
		Student3 stu21 = new Student3("Sandy", "MCA/07/19");
		Student3 stu31 = new Student3("Roxy", "MCA/07/32");

		// println internally
		// call toString
		// method
		System.out.println(stu11);
		System.out.println(stu21);
		System.out.println(stu31);
	}
}
