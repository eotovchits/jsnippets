package Strings;

/**
 * * This program is used to show the use of equals method. * @author
 * javawithease.
 */

class TestString2 {
	String str1 = "javawithease";
	String str2 = "javawithease";
	String str3 = new String("javawithease");
	String str4 = "jai";
	String str5 = "JAVAWITHEASE";

	/**
	 * * This method is used to compare strings using equals operator. * @author
	 * javawithease
	 */

	public void stringComparison() {
		// return true, because content are same.
		System.out.println(str1.equals(str2));
		System.out.println(str2.equals(str3));

		// return false, because content are not same.
		System.out.println(str2.equals(str4));

		// return false, because content are not same //(differ in case).
		System.out.println(str2.equals(str5));

		// return true, because content are same ignoring case.
		System.out.println(str2.equalsIgnoreCase(str5));
	}
}

public class StringComparisonExapmle2 {
	public static void main(String args[]) {
		// creating TestString object.
		TestString2 obj = new TestString2();

		// method call
		obj.stringComparison();
	}
}
