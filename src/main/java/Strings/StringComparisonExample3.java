package Strings;

/**
 * * This program is used to show the use of compareTo method. * @author
 * javawithease.
 */

class TestString3 {
	String str1 = "javawithease";
	String str2 = "javawithease";
	String str3 = "jai";
	String str4 = "sandy";

	/**
	 * * This method is used to compare * strings using compareTo operator.
	 * * @author javawithease
	 */
	public void stringComparison() {
		// return 0, because content are same.
		System.out.println(str1.compareTo(str2));

		// return greater than 0, because str1 //lexicographically greater than
		// str2.
		System.out.println(str1.compareTo(str3));

		// return less than 0, because str1 //lexicographically less than str2.
		System.out.println(str3.compareTo(str4));
	}
}

public class StringComparisonExample3 {
	public static void main(String args[]) {

		// creating TestString object.
		TestString3 obj = new TestString3(); // method call

		obj.stringComparison();
	}
}