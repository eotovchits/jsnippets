package Strings;

/**
 * * This program is used to show the use of == operator. * @author
 * javawithease.
 */

class TestString {
	String str1 = "javawithease";
	String str2 = "javawithease";

	String str3 = new String("javawithease");

	/**
	 * * This method is used to compare strings using == operator. * @author
	 * javawithease
	 */

	public void stringComparison() {
		// return true, because str1 and str2 both refers to the
		// same instance created in String constant pool.

		System.out.println(str1 == str2);

		// return false, because str3 refers to the
		// instance created in nonpool.
		System.out.println(str1 == str3);
	}
}

public class StringComparisonExample1 {
	public static void main(String args[]) {

		// creating TestString object.
		TestString obj = new TestString(); // method call
		obj.stringComparison();
	}
}
