package Strings;

/**
 * * This program is used to show the use of substring(int startIndex) * method
 * to get substrings of a given string. * @author javawithease
 */

class TestString4 {
	String str = "Hello javawithease";

	/**
	 * * This method is used to get substrings of a given string. * @author
	 * javawithease
	 */

	public void showSubString() {
		System.out.println(str.substring(6));
	}
}

class TestString5 {
	String str = "www.javawithease.com";

	/**
	 * * This method is used to get substrings of a given string. * @author
	 * javawithease
	 */
	public void showSubString() {
		System.out.println(str.substring(4, 16));
	}
}

public class SubStringExample1 {
	public static void main(String args[]) {
		// creating TestString object.
		TestString4 obj = new TestString4(); // method call
		obj.showSubString();

		TestString5 obj1 = new TestString5();
		// method call
		obj1.showSubString();
	}
}
