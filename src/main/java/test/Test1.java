package test;

/**
 * * This class is used to show use of Package class. * @author javawithease
 */
public class Test1 {
	public static void main(String args[]) {
		Package package1 = Package.getPackage("java.lang");
		System.out.println("package name = " + package1.getName());
		System.out.println("Specification Title = " + package1.getSpecificationTitle());
		System.out.println("Specification Vendor = " + package1.getSpecificationVendor());
		System.out.println("Specification Version = " + package1.getSpecificationVersion());
		System.out.println("Implementaion Title = " + package1.getImplementationTitle());	
	}
}
