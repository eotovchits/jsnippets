package Concurrency;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by host on 03.02.17.
 * <p>
 * http://www.javaworld.com/article/2071822/java-concurrency/book-excerpt--executing-tasks-in-threads.html
 */
public class WS2 {

    public static void main(String[] args) throws IOException {
        ServerSocket socket = new ServerSocket(80);
        while (true) {
            final Socket connection = socket.accept();
            Runnable task = new Runnable() {
                public void run() {
                    //handleRequest(connection);
                }
            };
            new Thread(task).start();
        }
    }
}
