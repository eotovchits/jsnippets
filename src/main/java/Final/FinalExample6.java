package Final;

/**
 * * This class is used to show the example of static blank final variable.
 * * @author javawithease
 */

class Test6 {
	// blank final variable which can only be initialize
	// through static initializer block.
	static final int num;

	static {
		num = 100;
		System.out.println("Num = " + num);
	}
}

public class FinalExample6 {
	public static void main(String args[]) {
		new Test6();
	}
}
