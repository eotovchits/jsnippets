package Final;

/**
 * * This program is used to show that final method can be inherited. * @author
 * javawithease
 */
class Show {
	public final void show() {
		System.out.println("Hello world.");
	}
}

class Display extends Show {
	public void display() {
		System.out.println("Hello javawithease.com.");
	}
}

public class FinalExample4 {
	public static void main(String args[]) {
		// creating object of Display class
		Display obj = new Display(); // method call
		obj.show();
	}
}
