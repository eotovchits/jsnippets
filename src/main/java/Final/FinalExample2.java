package Final;

/**
 * * This program is used to show that final method can't be override. * @author
 * javawithease
 */

class Show2 {
	public final void show() {
		System.out.println("Hello world.");
	}
}

class Display2 extends Show2 { // error because final method can't be override.
	/*
	public void show() {
		System.out.println("Hello javawithease.com.");
	}
	*/
}

public class FinalExample2 {
	public static void main(String args[]) {

		// creating object of Display class
		Display2 obj = new Display2(); // method call
		obj.show();
	}
}
