package Final;

/**
 * This class is used to show the example of blank final variable.
 * 
 * @author javawithease
 */
class Test {
	// blank final variable which can only be
	// initialize through constructor.
	final int num;

	Test(int n) {
		num = n;
		System.out.println("Num = " + num);
	}
}

public class FinalExample5 {
	public static void main(String args[]) {
		new Test(100);
	}
}
