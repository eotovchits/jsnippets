package Ctors;

public class ConstructorExample1 {
	int num;
	String str;

	ConstructorExample1() {
		System.out.println("Constructor called.");
	}

	public static void main(String args[]) {
		// constructor call
		ConstructorExample1 obj1 = new ConstructorExample1();
		// print default values of object
		// properties.
		System.out.println("num = " + obj1.num);
		System.out.println("str = " + obj1.str);
	}
}