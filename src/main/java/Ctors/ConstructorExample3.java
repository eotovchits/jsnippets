package Ctors;

public class ConstructorExample3 {
	int num;
	String str;

	ConstructorExample3(int n, String s) {
		System.out.println("Constructor called.");
		num = n;
		str = s;
	}

	public static void main(String args[]) { // constructor call
		ConstructorExample3 obj1 = new ConstructorExample3(10, "javawithease");
		// print values of object properties
		System.out.println("num = " + obj1.num);
		System.out.println("str = " + obj1.str);
	}
}
