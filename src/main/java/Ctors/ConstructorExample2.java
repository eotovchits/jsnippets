package Ctors;

public class ConstructorExample2 {
	int num;
	String str;

	public static void main(String args[]){
           //constructor call, compiler will automatically
           //creates the default constructor
           ConstructorExample2 obj1 = 
                          new ConstructorExample2();

           //print default values of object properties.
           System.out.println("num = " + obj1.num);
           System.out.println("str = " + obj1.str);
    }
}
