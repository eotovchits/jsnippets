package Ctors;

public class ConstructorExample4 {
	int num;
	String str;

	ConstructorExample4(int n, String s) {
		System.out.println("Constructor called.");
		num = n;
		str = s;
	}

	public static void main(String args[]) { // constructor call
		ConstructorExample4

		obj1 = new ConstructorExample4(10, "javawithease"); // error, because in
		// this case default constructor //will
		// not be provided by compiler.
		////ConstructorExample4 obj2 = new ConstructorExample4(); // print values
		// of object properties.
		////System.out.println("num = " + obj1.num);
		////System.out.println("str =" + obj1.str);
	}
}