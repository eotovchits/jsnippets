package Unsafe;

import sun.misc.Unsafe;

import java.lang.reflect.Field;

/**
 * Created by host on 22.11.16.
 */
public class T1 {

    static Unsafe theUnsafe;

    public static Unsafe getUnsafe() throws NoSuchFieldException, IllegalAccessException {
        /*
        Class cc = sun.reflect.Reflection.getCallerClass(2);
        if (cc.getClassLoader() != null)
            throw new SecurityException("Unsafe");
        */

        Field f = Unsafe.class.getDeclaredField("theUnsafe");
        f.setAccessible(true);
        Unsafe unsafe = (Unsafe) f.get(null);


        return theUnsafe;
    }


    static public void misc() throws InstantiationException, IllegalAccessException, NoSuchFieldException {

        A o1 = new A(); // constructor
        o1.a(); // prints 1

        A o2 = null; // reflection

        o2 = A.class.newInstance();

        o2.a(); // prints 1

        A o3 = (A) getUnsafe().allocateInstance(A.class); // unsafe
        o3.a(); // prints 0


    }

    public static void main (String args[])
    {
        try {
            misc();
            int aa = 345;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }
}
