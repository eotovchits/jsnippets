package Unsafe;

/**
 * Created by host on 22.11.16.
 */
class A {
    private long a; // not initialized value

    public A() {
        this.a = 1; // initialization
    }

    public long a() { return this.a; }
}

