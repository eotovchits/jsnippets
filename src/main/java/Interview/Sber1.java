package Interview;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * Created by SONY on 1/3/2017.
 */
public class Sber1 {

    public Map<String, Integer> toSet(List<String> list){

        Map<String, Integer> map = new TreeMap<>();

        for (String s : list){
            if (s != null)
                map.put(s, s.length());
        }

        return map;
    }

    public Map<String, Integer> toSet1(List<String> list){

        return list.stream().filter(Objects::nonNull).collect(Collectors.toMap(s -> s, String::length));

    }


}
