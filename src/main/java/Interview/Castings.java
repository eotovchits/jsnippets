package Interview;

/**
 * Created by SONY on 1/13/2017.
 */

class A{}

class B extends A{}

class C extends B{}

public class Castings {

    public static void main(String[] args){

        B bb = new B();

        C cc = (C)bb;

        A aa = new C();

        C ccc = (C)aa;
    }
}
